package Appointment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;
import java.util.ArrayList;

public class Appointment {
    String description;
    int month;
    int day;
    int year;

    public Appointment(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter an appointment description");
        description = input.nextLine();
        String loopExit = "running";
        while(loopExit.equals("running")) {
            try {
                String monthString;
                System.out.println("Enter an appointment month");
                monthString = input.nextLine();
                int monthInput = Integer.parseInt(monthString);
                if(monthInput > 12){throw new Exception();}
                month = monthInput;
                loopExit = "ending";
            } catch (Exception e) {System.out.println("The month input is not a number or number is greater than 12"); }
        }
        String dayExitLoop = "running";
        while(dayExitLoop.equals("running")) {
            try {
                String dayString;
                System.out.println("Enter an appointment day");
                dayString = input.nextLine();
                int dayInput = Integer.parseInt(dayString);
                if(dayInput > 31){throw new Exception();}
                day = dayInput;
                dayExitLoop = "ending";
            } catch (Exception e) {System.out.println("The day input is not a number or number is greater than 31"); }
       }
        String yearExitLoop = "running";
       while (yearExitLoop.equals("running")) {
            try {
                String yearString;
                System.out.println("Enter an appointment year");
                yearString = input.nextLine();
                int yearInput = Integer.parseInt(yearString);
                year = yearInput;
                yearExitLoop = "ending";
            } catch (Exception e) {System.out.println("The year input is not a number "); }// there is no case where i dont want my            //program to filter year
//////                                                                                            //I want to be able to add 1999 and 2025
        }
       }
    public static void save(String file, Appointment appointmentArg) throws IOException{
        try {
            File appointmentDB = new File(file);
            if( appointmentDB.createNewFile() ){
                System.out.println("Created a new file");
            }
            FileWriter fileWriter = new FileWriter("appointment.txt");
            fileWriter.write("description: ");
            fileWriter.write(appointmentArg.description);
            fileWriter.write("\r\n");

            String monthString = String.valueOf(appointmentArg.month);
            fileWriter.write("month: ");
            fileWriter.write(monthString);
            fileWriter.write("\r\n");


            String dayString = String.valueOf(appointmentArg.day);
            fileWriter.write("day: ");
            fileWriter.write(dayString);
            fileWriter.write("\r\n");


            String yearString = String.valueOf(appointmentArg.year);
            fileWriter.write("year: ");
            fileWriter.write(yearString);
            fileWriter.write("\r\n");

            fileWriter.write("\r\n");
            fileWriter.write("\r\n");
            fileWriter.close();

        }
        catch(Exception e){
            System.out.println("file could not be created");
        }

    }

    public static void main(String[] args) throws IOException {
        Appointment obj = new Appointment();
        save("appointment.txt",obj);
    }
}//class ending bracket


